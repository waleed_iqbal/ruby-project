module Processable

  def file_definition
    klass = self.class.superclass == Object ? self.class : self.class.superclass
    file_definition_attribute = klass.attachment_definitions.keys.first
    self.send(file_definition_attribute)
  end

  def process!(options)
    self.save(:validate => false)
    image = MiniMagick::Image.open(self.file_definition.path)
    if options[:direction].present?
      rotate(options[:direction])
    else
      scale(options, image)
    end
    self.save
  end

  def rotate(direction)
    image = MiniMagick::Image.open(self.file_definition.path)
    if image[:width] > 300
      canvas_resize!(:width => 300, :height => 250)
      image = MiniMagick::Image.open(self.file_definition.path)
    end
    if direction == "left"
      image.rotate("-90")
    else
      image.rotate("90")
    end
    image.write(file_definition.path)
  end

  def scale(options,image)
    width = options[:width]
    height = options[:height]
    xpos = options[:co_x]
    ypos = options[:co_y]
    image.crop("#{width}x#{height}+#{xpos.to_i}+#{ypos.to_i}")
    auto_resize(image)
    image.write(file_definition.path)
  end

  def auto_resize(image)
    width = image[:width] > 150 ? 150 : image[:width]
    height = image[:height] > 150 ? 150 : image[:height]
    directions = {:width => width, :height => height}
    resize(image, directions, false)
  end

  def canvas_resize!(directions)
    self.save
    image = MiniMagick::Image.open(self.file_definition.path)
    resize(image, directions)
    image.write(file_definition.path)
  end

  def resize(image, directions, preserve_ratio = true)
    resize_params = []
    resize_params << directions[:width].to_s
    resize_params << "x"
    resize_params << directions[:height].to_s
    resize_params << "!" unless  preserve_ratio
    image.coalesce
    image.resize resize_params.join
  end

end

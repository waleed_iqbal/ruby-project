class Hash
  def evaluate(*args)
    options = {:default => nil}.merge(args.last.is_a?(Hash) ? args.pop : {})
    target = self # Initial target is self.
    while target && key = args.shift
      target = target[key]
    end

    return target if target
    return options[:default]
  end

  def clean
    each do |k, v|
      if v.is_a?(Hash)
        v.clean
      elsif v.to_s.empty?
        delete(k)
      end
    end
  end

  def symbolize_keys_recursive
    inject({}) do |options, (key, value)|
      value = value.symbolize_keys_recursive if value.instance_of? Hash
      value = value.symbolize_keys_recursive if value.instance_of? Array
      options[key.to_sym || key] = value
      options
    end
  end
end

class String
  def ellipsisize(minimum_length=4,edge_length=3)
    return self if self.length < minimum_length or self.length <= edge_length*2
    edge = '.' * edge_length
    mid_length = self.length - edge_length*2
    gsub(/(#{edge}).{#{mid_length},}(#{edge})/, '\1...\2')
  end

  def to_sphinx_close_search_query
    "\"^#{self}$\" | \"#{self}\" | (#{self})"
  end

  def words
    self.split(/\s+/)
  end

  def urlize
    self.strip.downcase.gsub(/[^a-z0-9]+/, '-')
  end

  def underscored_urlize
    self.strip.downcase.gsub(/[^a-z0-9]+/, '_')
  end

  def shortize
    self.words.first.downcase
  end

  def to_slug
    to_url
  end
end

class Array
  def to_pretty_json
    ['[', self.collect {|e| e.to_json}.join(",\n"), ']'].join("\n")
  end

  def reject_blank
    self.reject(&:blank?)
  end

  def symbolize_keys_recursive
    inject([]) do |options, value|
      value = value.symbolize_keys_recursive if value.instance_of? Hash
      value = value.symbolize_keys_recursive if value.instance_of? Array
      options << value
      options
    end
  end
end

class Symbol
  def <=>(other)
    return to_s <=> other.to_s
  end
end

class Fixnum
  def in_mega_bytes
    to_f / (1024 * 1024)
  end

  def positive?
    self > 0
  end

end

class Numeric
  def percent_of(n)
     n * (self.to_f / 100.0)
  end
end

class Time
  def in_miliseconds
    (to_f * 1000).to_i
  end
  def formatted
    strftime("%b %d, %Y - %H:%M:%S")
  end
end
class DateTime
  def formatted_date
    strftime("%d/%m/%Y")
  end
end
class NilClass
  def <=>(other)
    return to_s <=> other.to_s
  end
end

class Object
  def to_human_time
    secs = self.to_i
    r = [[60, :second], [60, :minute], [24, :hour], [1000, :day]].collect do |count, name|
      secs, n = secs.divmod(count)
      # "#{n.to_i} #{name.to_s.pluralize(n.to_i)}" unless n.to_i.zero?
      "#{n.to_i}#{name.to_s.first}" unless n.to_i.zero?
    end.compact.reverse.join(' ')

    r.present? && r || "0m"
  end

  def is?(value)
    self == value
  end

  def is_not?(value)
    self != value
  end

  def indefinite_articlerize(capitalize = false)
    vowels = %w(a e i o u)
    a_word  = capitalize && "A" || "a"
    an_word = capitalize && "An" || "an"

    return "#{an_word} #{self}" if vowels.include?(self.first.downcase)
    return "#{a_word} #{self}"
  end

  def to_e
    self unless self.blank?
  end

  def to_js_time
    self.to_time.to_i
  end

  def to_custom_time
    self.strftime("%H:%M %p UTC")
  end

  def to_normal_datetime
    [self.to_normal_time, self.to_date.to_s].join(' on ')
  end

  def to_normal_time
    self.strftime("%H:%M %p")
  end


  def rcall(*args)
    options = {:d => nil}.merge(args.last.is_a?(Hash) ? args.pop : {})
    target = self

    while target && m = args.shift
      target = target.send(m) if target.respond_to?(m)
    end

    return target if target
    return options[:d].call if options[:d].respond_to? :call
    return options[:d]
  end
end

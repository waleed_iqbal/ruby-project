class Mailchimp

  LIST_NAME = "Postbord 2"

  class << self
    def api
      Gibbon::API.new APP_CONFIG[:mailchimp_api_key]
    end

    def extract_data_from(response)
      response.symbolize_keys_recursive[:data]
    end

    def lists
      extract_data_from(api.lists.list)
    end

    def list(list_name)
      lists = extract_data_from(api.lists.list({:filters => {:list_name => list_name}}))
      lists.first
    end

    def members(list_name)
      my_list = list(list_name)
      extract_data_from(api.lists.members({:id => my_list[:id]}))
    end

    def subscribed?(email, list_name)
      members(list_name).collect do |member|
        member[:email] == email
      end.any?
    end

    def subscribe!(email, list_name)
      api.lists.subscribe({:id => list(list_name)[:id], :email => {:email => email}, :double_optin => false}) unless subscribed?(email, list_name)
    end

  end

end

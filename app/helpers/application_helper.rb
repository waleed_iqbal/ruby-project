module ApplicationHelper

  include Rack::Recaptcha::Helpers

  def blank_layout?
    controller.send(:_layout).virtual_path.name == "blank"
  end

  def close_colorbox_reload_page(id = nil)
    raw render(:partial => "shared/colorbox_close", :locals => {:id => id})
  end

  def month_names
    ["January" ,"February" ,"March" ,"April" ,"May" ,"June" ,"July" ,"August" ,"September" ,"October" ,"November" ,"December"]
  end

  def cuurent_env
    APP_CONFIG[:env] || Rails.env
  end

  def browser_env
    @user_agent.mobile? ? "mobile" : "desktop"
  end

  def browser_os
    @user_agent.os.gsub(/\s/,"-").underscore
  end

  def browser_name
    @user_agent.browser.underscore
  end

  def colorbox_cancel_link
    link_to "Cancel", "#", :class => "cancel", :onclick => "parent.$.fn.colorbox.close()"
  end

  def year_select(f, options)
    f.select options[:name], options_for_select(options[:select_options], f.object.send(options[:name]) || Time.now.year), :include_blank => options[:blank_text]
  end

  def page_title
    titles = []
    if title.present?
      titles << title
      titles << " - "
    end
    titles << "Postbord"
    titles.join
  end

  def title
    @title || title_from_yml
  end

  def title_from_yml
    return if PAGE_TITLE[:titles].blank?
    PAGE_TITLE[:titles].evaluate(controller.controller_name.to_sym,controller.action_name.to_sym)
  end

  def meta_description
    @meta_description
  end

  def country_name_from_ip(ip = nil)
    country = GEO_IP.country(ip || request.ip)
    country.try :country_name
  end

  def link_with_font_awesome(url, icon_class, options)
    link_to url, options do
      font_awesome_icon(icon_class)
    end
  end

  def font_awesome_icon(icon_class)
    content_tag(:i, "", :class => icon_class)
  end

  def header_menu
    return link_to(font_awesome_icon("fa fa-user fa-2x"), login_path, class: 'colorbox login-popup' ) unless user_signed_in?
    render :partial => "layouts/menu"
  end

  def active_class(name)
    return "active" if name == controller.action_name
  end

  def profile_active_class(name)
    return "active_link" if name == controller.action_name
  end
  
end

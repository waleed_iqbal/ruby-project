module EmailsHelper

  def link_or_primary(email)
    if email.primary?
      primary_text.html_safe
    else
      primary_link(email)
    end
  end

  def primary_text
    content_tag(:span, "Primary email", :class => "primary-email")
  end

  def primary_link(email)
    link_to "Make primary", make_primary_profile_email_path(@user, email), :method => :post, :class => "primary-email-link"
  end

  def link_to_remove_email(email)
    link_to "Remove", profile_email_path(@user, email), :method => :delete, :confirm => "Are you sure?"
  end

end

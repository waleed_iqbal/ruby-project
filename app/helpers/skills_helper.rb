module SkillsHelper

  def user_skills
    @user.skills.collect(&:name).join(",")
  end

end

module ProfilesHelper

  def profile_image
    html = []
    edit_tag = []
    if @user.avatar.blank?
      image_url = "default-img.png"
    else
      image_url = @user.avatar.url
    end
    html << image_tag(image_url, :class => "avatar")
    edit_tag << image_tag(image_url)
    edit_tag << link_to(edit_image_profile_path(@user)) do
      content_tag(:i, "", :class => "fa fa-camera fa-2x")
    end
    html << content_tag(:div, :class => "edit-avatar hidden") do
      edit_tag.join.html_safe
    end.html_safe
    html.join.html_safe
  end

  def avatar_url
    if @image.present?
      @image.photo.url
    elsif @user.avatar.present?
      @user.avatar.url
    else
      'http://placehold.it/150&text=Preview'
    end
  end

  def image_file
    url = @image.present? ? @image.photo.url : user_image
    url = [url.split("?")[0], (1+rand(1000000000)).to_s].join("?")
    image_tag(url, :class => "img-load")
  end

  def user_image
    return "http://placehold.it/300x250&text=Upload an image" if @user.avatar.blank?
    @user.avatar.url
  end

  def primary_email_for(user)
    [user.primary_email].join.html_safe
  end

  def profile_completeness_prompt
    return if cookies[:incomplete_profile].present?
    render :partial => "profiles/incomplete" if @user.profile_completeness < 60
  end

  def url_with_protocol(external_url)
    /^http/.match(external_url) ? external_url : "http://#{external_url}" if external_url.present?
  end

  def profile_social_urls
    [:twitter, :github, :personal_website, :linkedin].collect do |network|
      klass = "globe" if network.is?(:personal_website)
      profile_social_url(network, klass)
    end.join.html_safe
  end

  def profile_social_url(network, klass = nil)
    link_to url_with_protocol(current_user.profile.send(network)), title: network.to_s.titleize, target: '_blank' do
      font_awesome_icon("fa fa-#{klass || network} fa-2x")
    end
  end

end

module ExperiencesHelper

  def end_date_of(experience)
    experience.current ? "Present" : experience_date(experience.end_date)
  end

  def experience_date(date)
    date.strftime("%B %Y")
  end

  def duration_for_experience(experience)
    start_date = experience.start_date
    if experience.current
      end_date = Time.current
    else
      end_date = experience.end_date
    end
    time_diff_components = Time.diff(start_date, end_date, '%y, %M')
    time_diff_components[:diff]
  end

  def remove_experience_link
    link_to remove_profile_experience_path(@user, @experience) do
      content_tag(:i, "", :class => "fa fa-trash-o fa-2x")
    end.html_safe
  end

  def remove_certificate_link
    link_to remove_profile_certificate_path(@user, @certificate) do
      content_tag(:i, "", :class => "fa fa-trash-o fa-2x")
    end.html_safe
  end

  def remove_education_link
    link_to remove_profile_education_path(@user, @education) do
      content_tag(:i, "", :class => "fa fa-trash-o fa-2x")
    end.html_safe
  end


  def visibility_class(experience)
    return "hidden" if experience.removed?
    "visible"
  end

end

module EducationsHelper

  def enrollment_year_range
    (1900..Time.now.year).to_a
  end

  def graduation_year_range
    (1900..2020).to_a
  end

end

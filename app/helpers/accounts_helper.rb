module AccountsHelper

  def connected_account(type)
    new_type = type.split("-").first
    connected = is_connected?(new_type)
    link_to url_for_social_connectivity(new_type, connected), :class => "pull-left link-connected #{ability_class(connected)}" do
      content_tag :div, :class => "social-account" do
        [content_tag(:i, "", :class => "fa fa-#{type} fa-2x fa-social"), content_tag(:i, "", :class => "fa #{connected_check_class(connected)} fa-2x")].join.html_safe
      end
    end
  end

  def connected_check_class(connected)
    return "fa-check-square-o" if connected
    "fa-square-o"
  end

  def is_connected?(type)
    @user.authentications.where(:provider => type).present?
  end

  def ability_class(connected)
    return "disabled" if connected
  end

  def url_for_social_connectivity(type, connected)
    return "#" if connected
    social_path(type)
  end

  def country_code_selector
    Country.all.collect do |country|
      [country.name.titleize, country.id]
    end
  end

end

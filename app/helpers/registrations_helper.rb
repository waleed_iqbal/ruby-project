module RegistrationsHelper

  def captcha_tag
    return if development?
    html = []
    html << label_tag("Prove you're not a robot")
    html << recaptcha_tag(:challenge, :display => { :theme => 'clean'})
    content_tag :div, :class => "row captcha-holder" do
      html.join.html_safe
    end.html_safe
  end

end

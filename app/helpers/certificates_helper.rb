module CertificatesHelper

  def link_to_preview(certificate)
    return if certificate.attachment.blank?
    content_tag :div, :class => "preview-link" do
      link_to preview_profile_certificate_path(@user, certificate), :class => "colorbox", :id => "certificate_#{certificate.id}" do
        image_tag ["icons/",certificate.attachment.content_type.urlize, ".png"].join
      end
    end
  end

end

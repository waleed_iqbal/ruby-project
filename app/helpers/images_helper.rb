module ImagesHelper

  def preview_link
    link_with_font_awesome("#", "fa fa-refresh", :class => "preview")
  end

  def remove_link
    link_with_font_awesome("#", "fa fa-times", :onclick => "location.reload();return false;")
  end

  def trash_link
    link_with_font_awesome(remove_avatar_profile_path(@user), "fa fa-trash-o", :method => :delete, :confirm => "Are you sure?")
  end

  def rotate_clockwise_link
    link_with_font_awesome("#", "fa fa-rotate-right", :class => "rotate-btn", "data-direction" => "right")
  end

  def rotate_anti_clockwise_link
    link_with_font_awesome("#", "fa fa-rotate-left", :class => "rotate-btn", "data-direction" => "left")
  end

  def tick_link
    link_with_font_awesome("#", "fa fa-check", :class => "preview")
  end

end

(function($) {
  /* Colorbox resize function */
  var resizeTimer;
  function resizeColorBox()
  {
      var myWidth = 960, percentageWidth = .95;
      if (resizeTimer) clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {
              if (jQuery('#colorbox').is(':visible')) {
                      jQuery.colorbox.resize({ width: ( $(window).width() > ( myWidth+20) )? myWidth : Math.round( $(window).width()*percentageWidth ) });
              }
      }, 300)
  }
  // Resize Colorbox when resizing window or changing mobile device orientation
  jQuery(window).resize(resizeColorBox);
  window.addEventListener("orientationchange", resizeColorBox, false);
}(jQuery));
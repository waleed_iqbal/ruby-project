window.click_profile_experience_link = ->
  $("#profile_experience").click()

daydiff = (first, second) ->
  (second - first) / (1000 * 60 * 60 * 24)

parseDate = (str) ->
  mdy = str.split("/")
  new Date(mdy[2], mdy[0] - 1, mdy[1])

$(document).ready ->
  if $("#experience_current:checked").length > 0
    $("#experience_end_date, #end_date").toggleClass("disabled")

  $("#experience_current").click ->
    $("#experience_end_date, #end_date").toggleClass("disabled")

  $("#start_date").datepicker
    dateFormat: "mm/yy"
    altField: "#experience_start_date"
    altFormat: "dd/mm/yy"
    maxDate: "0D"

  $("#end_date").datepicker
    dateFormat: "mm/yy"
    altField: "#experience_end_date"
    altFormat: "dd/mm/yy"
    maxDate: "0D"

    minDate = new Date($("#experience_start_date").val())
    minDate.setDate minDate.getDate() + 1
    $("#end_date").datepicker "option", "minDate", minDate

  $("#experience_company").autocomplete
    source: "/suggestions?from=Experience&for=company"

  $("#experience_title").autocomplete
    source: "/suggestions?from=Experience&for=title"

  $("#experience_location").autocomplete
    source: "/suggestions?from=Experience&for=location"

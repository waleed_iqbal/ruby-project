window.validate_email_presence = ->
  $(".email").blur ->
    $.ajax
      url: "/email_validation",
      data:
        email: $(".email").val()
      success : (data) ->
        unless data.validated
          $(".email").siblings(".LV_validation_message").text(data.message).removeClass("LV_valid").addClass("LV_invalid")
        else
          $(".email").siblings(".LV_validation_message.LV_valid").text("")


window.validate_attachment_type = (field, allowed_types) ->
  $(field).change ->
    file_type = this.files[0].type
    unless $.inArray(file_type, allowed_types) >= 0
      $(this).siblings(".formError").text("Please select a valid format")
    else
      $(this).siblings(".formError").text("")


window.set_live_validations = ->

  if $("section.create-account").length > 0

    $('#user_first_name').validate_action({action: "name"})
    $('#user_last_name').validate_action({action: "name"})
    $('#user_password').validate_action({action: "password_length"})
    $('#user_password_confirmation').validate_action({action: "password_confirmation", with: "user_password"})

  if $("#user_email").length > 0
    $('#user_email').validate_action({action: "email_format"})
    $('#user_email').validate_action({action: "email_presence"})

  if $("#confirmation_email").length > 0
    $('#confirmation_email').validate_action({action: "email_format"})
    $('#confirmation_email').validate_action({action: "email_presence"})

  if $("#new_user").length > 0
    $('#user_password').validate_action({action: "password_length"})
    $('#user_password_confirmation').validate_action({action: "password_confirmation", with: "user_password"}) if $('#user_password_confirmation').exists()

$(document).ready ->
  validate_email_presence() if $("section.create-account").exists() || $(".edit_user").exists()
  set_live_validations()

  if $(".edit_user").exists()

    $('#user_name').validate_action({action: "presence", message: "Please enter full name"})
    $('#user_name').validate_action({action: "name"})

    $('#user_email').validate_action({action: "email_format"})
    $('#user_email').validate_action({action: "email_presence"})

  if $(".email_for_news_letter").exists()
    $('.email_for_news_letter').validate_action({action: "email_format"})
    $('.email_for_news_letter').validate_action({action: "email_presence"})

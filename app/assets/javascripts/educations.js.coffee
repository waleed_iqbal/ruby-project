$(document).ready ->

  $("#education_school").autocomplete
    source: "/suggestions?from=Education&for=school"

  $("#education_degree").autocomplete
    source: "/suggestions?from=Education&for=degree"

  $("#education_grade").autocomplete
    source: "/suggestions?from=Education&for=grade"

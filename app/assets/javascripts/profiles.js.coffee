window.jcrop_api = null

window.load_social_welcome = ->
  if ($.cookie("social_connect") == "" || $.cookie("social_connect") == undefined) && $(".social-form").exists()
    $.colorbox
      html: $(".social-form").html()
      innerWidth: "50%"
      innerHeight: "35%"
      escKey: false
      overlayClose: false
      onClosed: ->
        location.href = "/main/set_cookie?value=skipped" if ($.cookie("social_connect") == "" || $.cookie("social_connect") == undefined)

window.preview_image = ->
  width  = jcrop_api.tellSelect().w
  height = jcrop_api.tellSelect().h
  xpos   = "-" + jcrop_api.tellSelect().x + "px"
  ypos   = "-" + jcrop_api.tellSelect().y + "px"
  $("#preview").css("width",width)
  $("#preview").css("height",height)
  $("#preview img").attr "src", $('section.show .img-load').attr("src")
  $("#preview img").css("left",xpos)
  $("#preview img").css("top",ypos)

window.setValue = (selection) ->
  preview_image()
  $("#x").val(selection.x)
  $("#x2").val(selection.x2)
  $("#y").val(selection.y)
  $("#y2").val(selection.y2)
  $("#width").val(selection.w)
  $("#height").val(selection.h)

window.set_image_resizeable = ->
    unless jcrop_api == null
      jcrop_api.destroy()
    $('section.show .img-load').Jcrop
      minSize: [150, 150]
      aspectRatio: 1
      onSelect: setValue
      ->
        window.jcrop_api = this

window.set_image_handlers = ->
  unless jcrop_api == null
    jcrop_api.destroy()

  $('.image-file').change ->
    formData = new FormData($("form")[0])
    formData.append("method", "upload")
    $.ajax
      url: "/images/upload_image"
      type: "POST"
      data: formData
      processData: false
      contentType: false
      cache: false

      beforeSend: ->
        $(".jcrop-holder").hide()
        $(".img-load").show().replaceWith('<i class="fa-li fa fa-spinner fa-spin fa-4x"></i>')
      success: (data) ->
        $("section").replaceWith(data)
        $("#profile-image").val("")
        set_image_resizeable()
        preview_image()

  $("a.rotate-btn").click ->
    formData = new FormData($("form")[0])
    formData.append("direction",$(this).attr("data-direction"))
    formData.append("method", "rotate")
    $.ajax
      url: "/images/upload_image"
      type: "POST"
      data: formData
      processData: false
      contentType: false
      cache: false

      beforeSend: ->
        $(".jcrop-holder").hide()
        $(".img-load").show().replaceWith('<i class="fa-li fa fa-spinner fa-spin fa-4x"></i>')
      complete: (data) ->
        $("section").replaceWith(data.responseText)
        set_image_resizeable()
        preview_image()


    return false

  $("a.preview").click ->
    preview_image()
    return false

$(document).ready ->
  $("#password-link-colorbox").colorbox
    iframe: true
    innerWidth: "50%"
    innerHeight: "40%"

  $("#email-link-colorbox").colorbox
    iframe: true
    innerWidth: "50%"
    innerHeight: "42%"

  $(".edit-avatar a").colorbox
    iframe: true
    innerWidth: "75%"
    innerHeight: "62%"

  $(".incomplete-edit-avatar a").colorbox
    iframe: true
    innerWidth: "75%"
    innerHeight: "100%"

  $(".avatar").hover ->
    $(this).addClass("hidden")
    $(".edit-avatar").removeClass("hidden")
    $(".edit-avatar").mouseout ->
      $(this).addClass("hidden")
      $(".avatar").removeClass("hidden")

  $(".incomplete-avatar").hover ->
    $(this).addClass("hidden")
    $(".incomplete-edit-avatar").removeClass("hidden")
    $(".incomplete-edit-avatar").mouseout ->
      $(this).addClass("hidden")
      $(".incomplete-avatar").removeClass("hidden")

  set_image_handlers()
  set_image_resizeable()
  validate_attachment_type(".image-file",["image/gif","image/png","image/jpeg","image/jpg"])

  $(".add-experience-colorbox").colorbox({iframe: true, innerWidth: "50%", innerHeight: "80%"})
  $(".add-education-colorbox").colorbox({iframe: true, innerWidth: "50%", innerHeight: "80%"})
  $(".add-certificate-colorbox").colorbox({iframe: true, innerWidth: "50%", innerHeight: "80%"})
  $(".dial").knob()

  $(".dropdown-profile").click ->
    $(".profile-menu").toggle();
  $(".header-dropdown").click ->
    $(".profile-menu").hide();
  $("#profile_experience").click ->
   $(".profile-menu").hide();
  $("#profile_certificate").click ->
    $(".profile-menu").hide();
  $("#profile_education").click ->
    $(".profile-menu").hide();
  $("#profile_skills").click ->
    $(".profile-menu").hide();
  $(document).click ->
    $(".profile-menu").hide();

window.preview_certificate = (e) ->
  $.colorbox
    html: $("").html()

$(document).ready ->

  $("#certificate_attachment").change ->
    $("#certificate-attachment span").text $(this).val()

  $("#certificate_name").autocomplete
    source: "/suggestions?from=Certificate&for=name"

  $("#certificate_authority").autocomplete
    source: "/suggestions?from=Certificate&for=authority"

  if $("#certificate_attachment").length > 0
    validate_attachment_type("#certificate_attachment",["image/gif","image/png","image/jpeg","image/jpg","application/pdf"])

  $(".edit_job input.save").click (e) ->
    preview_certificate(certificate)

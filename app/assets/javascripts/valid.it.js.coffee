(($) ->
  $.fn.validate_action = (options) ->

    validate_presence = (elem, message) ->
      validation = new LiveValidation(elem);
      validation.add( Validate.Presence, {failureMessage: message} )

    validate_name = (elem) ->
      name_validation = new LiveValidation(elem);
      name_validation.add( Validate.Presence, {failureMessage: "Please enter your name."} )
      name_validation.add( Validate.Format, { pattern: /^[a-zA-Z0-9-]+[a-zA-Z\s-]+[a-zA-Z0-9-]*$/i, failureMessage: "Name may contain only alphabets and hyphen (-)." } )
      name_validation.add( Validate.Length, { minimum: 2, tooShortMessage: "Name field must contain more than 2 letters." } )

    validate_password_length = (elem) ->
      password = new LiveValidation(elem)
      password.add( Validate.Length, { minimum: 6, tooShortMessage: "Password must be at least 6 characters." } )
      password.add( Validate.Presence, {failureMessage: "Please enter your password."} )

    validate_passwrd_confirmation = (elem, matching_element) ->
      password_confirmation = new LiveValidation(elem)
      password_confirmation.add( Validate.Presence, {failureMessage: "Please enter your password confirmation."} )
      password_confirmation.add( Validate.Confirmation, { match: matching_element, failureMessage: "Password entries do not match, please try again." } )

    validate_email_format = (elem) ->
      email = new LiveValidation(elem, {onlyOnBlur: true});
      email.add( Validate.Email, {failureMessage: "Email doesn't appear valid." } )

    validate_email_presence = (elem) ->
      email_presence = new LiveValidation(elem);
      email_presence.add( Validate.Presence, {failureMessage: "An email is required."} )

    settings = $.extend(
      valid: true
    , options)

    validate_presence(this.attr("id"), settings.message) if settings.action == "presence"
    validate_name(this.attr("id")) if settings.action == "name"
    validate_password_length(this.attr("id")) if settings.action == "password_length"
    validate_passwrd_confirmation(this.attr("id"),settings.with) if settings.action == "password_confirmation"
    validate_email_format(this.attr("id")) if settings.action == "email_format"
    validate_email_presence(this.attr("id")) if settings.action == "email_presence"

    return

) jQuery

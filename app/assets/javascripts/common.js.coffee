$(document).ready ->
  $(".password-strength").strength
    strengthButtonText: ""

  $(".colorbox").colorbox();

  $(window).hashchange ->
    return if location.hash == ""

    if location.hash == "#current"
      location.hash = ""
      location.reload()

    hash = location.hash
    $(hash).click()
    location.hash = ""

  load_social_welcome()

  $('#slider-code').tinycarousel()

  tinyMCE.init
    selector: 'textarea.editor'
    content_css: '/assets/tinymce.css'

  $(".datepicker").datepicker
    dateFormat: "dd/mm/yy"

  $(".login-popup").colorbox
    iframe: true
    height: "70%"
    width: "100%"
  $.colorbox.close()
  $("iframe:not([visibility='visible']").hide()
  $("#viewer").show()

has_text = (iteration, text) ->
  content = $(iteration).map(->
    $(this).text().replace(/\s/g, "").toLowerCase()
  )
  $.inArray(text.toLowerCase(), content) >= 0

$(document).ready ->

  $("#skills").autocomplete source: "/suggestions?from=Skill&for=name"
  $("#tags").tagsInput()
  $("#completed_tags").tagsInput()

  $("#addtag").click ->
    $("#tags").addTag $("#skills").val().replace(/\s/g, "")  if $(".skills-form .tag span").length < 36 and $("#skills").val().replace(/\s/g, "") isnt "" and not has_text($(".tag span"), $("#skills").val().replace(/\s/g, ""))
    $("#skills").val ""
    false

 $(".add-skills-colorbox").click (e) ->
   $(".skills-form").show()
   $(".skills").show()
   e.preventDefault()
   false

window.populate_search_field = (input) ->
  $("#search_query").val $(input).text()

window.lookup = (inputString) ->
  if inputString.length < 3
    $("#suggestions").fadeOut() # Hide the suggestions box
  else
    $.get "/suggestions", # Do an AJAX call
      queryString: "" + inputString + ""
    , (data) ->
      $("#suggestions").fadeIn() # Show the suggestions box
      $("#suggestions").html data # Fill the suggestions box


$(document).ready ->
  $(".search-form form").on "ajax:before", ->
    $("#spinner, .results").toggle()

  google.setOnLoadCallback ->
    $("input").blur ->
      $("#suggestions").fadeOut()


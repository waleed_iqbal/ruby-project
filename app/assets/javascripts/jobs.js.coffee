window.update_confirmation = ->
  $.colorbox
    html: $("#dialog-confirm").html()

window.delete_confirmation = (e) ->
  $.colorbox
    html: $(e.target).siblings(".delete-confirm").html()

$(document).ready ->

  $("#job_country_id").change ->
    $(this).siblings("strong").text $(this).find("option:selected").text()

  $(".how-to-apply input:radio:checked").click()

  $(".edit_job input.save").click (e) ->
    update_confirmation()
    e.preventDefault()

  $(".destroy_job").click (e) ->
    delete_confirmation(e)
    e.preventDefault()
    false

  $("#job_linkedin_application_method").click ->
    $("#job_notification_email").attr("disabled", false)
    $("#job_application_url").attr("disabled", true)

  $("#job_external_application_method").click ->
    $("#job_application_url").attr("disabled", false)
    $("#job_notification_email").attr("disabled", true)

$(document).ready ->

  $("#job_company_name").autocomplete
    source: "/suggestions?from=Company&for=name"

  $("#job_title").autocomplete
    source: "/suggestions?from=Job&for=title"

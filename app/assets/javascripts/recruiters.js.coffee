$(document).ready ->

  if $("#new_recruiter").length > 0

    $('#recruiter_company').validate_action({action: "presence", message: "Please enter company name"})
    $('#recruiter_title').validate_action({action: "presence",message: "Please enter job title"})

    $('#recruiter_first_name').validate_action({action: "name"})
    $('#recruiter_last_name').validate_action({action: "name"})

    $('#recruiter_email').validate_action({action: "email_format"})
    $('#recruiter_email').validate_action({action: "email_presence"})

class Country
  include Mongoid::Document

  field :name, type: String
  field :country_code, type: String
  field :dialing_code, type: String

  has_many :jobs

  default_scope order_by('name ASC')

end

class Authentication

  include Mongoid::Document
  include Mongoid::Timestamps

  field :access_id, :type => String
  field :access_token, :type => String
  field :oauth_token, :type => String
  field :provider, :type => String
  field :user_id, :type => Integer
  field :screen_name, :type => String

  attr_accessible :access_id, :access_token, :oauth_token, :provider, :user_id

  belongs_to :user

  def self.from_provider(provider, data)
    send(provider, *[data])
  end

  def self.linkedin(data)
    email = data[:user].delete(:email_address)
    user = data.delete(:existing_user) || User.where(:email => email).first || User.new(:email => email)
    auth = Authentication.where(:access_id => data[:auth_token], :access_token => data[:auth_secret]).first
    auth ||= Authentication.new(:access_id => data[:auth_token], :access_token => data[:auth_secret], :provider => 'linkedin')
    user.attributes = data[:user]
    auth.update_user_info(user)
  end

  def self.facebook(data)
    user = data.delete(:existing_user) || User.where(:email => data.email).first || User.new(:email => data.email)
    auth = user.facebook_authentication || Authentication.new(:access_token => data.access_token, :provider => 'facebook')
    user.first_name = data.first_name
    user.last_name = data.last_name
    auth.update_user_info(user)
  end

  def self.twitter(data)
    auth = Authentication.where(:access_id => data.oauth_token, :access_token => data.oauth_token_secret).first
    auth ||= Authentication.new(:access_id => data.oauth_token, :access_token => data.oauth_token_secret, :provider => 'twitter')
    user = data.delete(:existing_user) || auth.user || User.new
    auth.screen_name = data.screen_name
    auth.update_user_info(user)
  end

  def self.google(data)
    auth = Authentication.where(:access_id => data[:id], :provider => 'google').first
    auth ||= Authentication.new(:access_id => data[:id], :provider => 'google')
    user = data.delete(:existing_user) || auth.user || User.new
    user.first_name = data[:given_name]
    user.last_name = data[:family_name]
    auth.update_user_info(user)
  end

  def update_user_info(new_user)
    activity_type = new_user.persisted? ? 'sign in' : 'sign up'
    new_user.confirmed_at = Time.now
    new_user.save(:validate => false)
    self.user = new_user
    self.save
    [self.user,activity_type]
  end

end

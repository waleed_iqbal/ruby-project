class Activity
  include Mongoid::Document
  include Mongoid::Timestamps

  field :activity_mode, :type => String
  field :activity_type, :type => String
  field :browser, :type => String
  field :os, :type => String
  field :user_id, :type => Integer
  field :ip, :type => String
  field :country, :type => String
  field :area, :type => String
  field :city, :type => String

  attr_accessible :activity_mode, :activity_type, :browser, :os, :user_id, :ip, :country, :city, :area

  belongs_to :user

  def as_json(options = {})
    {
      :activity =>
      {
        :activity_type => activity_type,
        :activity_mode => activity_mode,
        :geo_location => {
          :ip => ip,
          :location => {:area => area, :city => city, :country => country}
        },
        :device => {
          :operating_system => os,
          :browser => browser
        },
        :timestamp => created_at.formatted
      }
    }
  end

end

class Certificate

  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip

  field :name, type: String
  field :authority, type: String
  field :month, type: String
  field :year, type: Integer
  field :user_id, type: Integer
  field :hidden, type: Boolean

  scope :removed, where(hidden: true)

  belongs_to :user

  def hide!
    self.hidden = true
    self.save
  end

  def removed?
    hidden
  end

  has_mongoid_attached_file :attachment,
                            :path => ":rails_root/public/assets/attachments/:id/:style/:basename.:extension",
                            :url  => "/assets/attachments/:id/:style/:basename.:extension"

  validates :name, :authority, :month, :year, :presence => true
  validates :year, :numericality => {:less_than_or_equal_to => 2020, :greater_than_or_equal_to => 1900}
  validates :year, :length => { :is => 4 }

  validate :current_date

  validates_attachment_content_type :attachment, :content_type => ['image/jpeg', 'image/png', 'image/gif', 'application/pdf'], :message => "Please select a valid format"

  def current_date
    return if month.blank? || year.blank?
    date = DateTime.strptime([month,year].join(" "), '%B %Y')
    if date > Time.now
      errors.add(:current_date, "Date cannot be in future")
      return false
    end
    true
  end

end

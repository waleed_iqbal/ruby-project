class User

  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paperclip
  include Processable

  has_mongoid_attached_file :avatar,
                            :path => ":rails_root/public/assets/images/avatars/:id/:style/:basename.:extension",
                            :url  => "/assets/images/avatars/:id/:style/:basename.:extension"

  validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png', 'image/gif'], :message => "Please select a valid format"

  before_save :update_email
  after_create :subscribe_me_for_news_letter

  attr_accessor :subscribe_to_newsletter

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable

  ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time
  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  field :confirmation_token,   :type => String
  field :confirmed_at,         :type => Time
  field :confirmation_sent_at, :type => Time
  field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  field :locked_at,       :type => Time
  field :update_token,    :type => String

  field :first_name, :type => String
  field :last_name, :type => String
  field :industry_id, :type => String

  ## Token authenticatable
  # field :authentication_token, :type => String

  has_many :authentications, :dependent => :destroy
  has_many :activities, :dependent => :destroy
  has_many :experiences, :dependent => :destroy
  has_many :certificates, :dependent => :destroy
  has_many :educations, :dependent => :destroy
  has_many :skills, :dependent => :destroy
  has_many :emails, :dependent => :destroy
  has_many :images, :as => :imageable, :dependent => :destroy
  has_one  :profile, :dependent => :destroy
  has_many  :jobs, :dependent => :destroy

  belongs_to :industry

  after_create :set_primary_email
  after_create :add_profile

  def name
    name_string = []
    name_string << self.first_name
    name_string << " " if self.first_name.present?
    name_string << self.last_name
    name_string.join
  end

  def name=(value)
    self.first_name, self.last_name = value.split(" ")
  end

  def has_max_failed_attempts?
    user = User.where(:email => self.email).first
    user.try(:has_three_failed_attempts?)
  end

  def has_three_failed_attempts?
    failed_attempts > 2
  end

  def facebook_authentication
    authentications.where(:provider => 'facebook').first
  end

  def update_skills(tags)
    self.skills.destroy_all
    tags.split(",").each do |tag|
      skills << Skill.new(:name => tag.strip)
    end
  end

  def update_primary_email!(email_address)
    self.emails.each do |address|
      address.primary = false
      address.save
    end
    self.email = email_address.address
    self.skip_reconfirmation!
    self.save
    email_address.primary = true
    email_address.save
  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if new_email = conditions.delete(:email)
       mail = Email.where(:address => new_email, :confirmed => true).first
       mail.try(:user)
    else
      where(conditions).first
    end
  end

  def send_password_update_notification(ip = nil)
    self.update_token = SecureRandom.hex
    self.save(:validate => false)
    Notification.password_update(self, ip).deliver
  end

  def send_password_instructions
    self.update_reset_token!
    Notification.password_instructions(self).deliver
  end

  def update_reset_token!
    self.reset_password_token = User.reset_password_token
    self.reset_password_sent_at = Time.now
    self.save
  end

  def recruiter?
    self.class.to_s == "Recruiter"
  end

  def email_changed?
    changed? && changes.symbolize_keys[:email].present?
  end

  def update_email
    return if self.new_record?
    if email_changed?
      email = emails.where(:primary => true).first
      email = Email.new(:primary => true) if email.blank?
      email.address = self.changes.symbolize_keys[:email].last
      self.emails << email
    end
  end

  def primary_email
    emails.where(:primary => true).first.try(&:address)
  end

  def company_name
    self.company
  end

  def profile_completeness
    completeness_attributes = {:skills => 12, :educations => 25, :experiences => 55, :certificates => 7, :avatar => 1}
    percentage = 0
    completeness_attributes.each do |attr, value|
      percentage += value unless self.send(attr).blank?
    end
    percentage
  end

  def current_job_title
    self.experiences.currently_working.latest.first.title
  end

  def currently_working
    self.experiences.currently_working.collect(&:company).join(",")
  end

  def previously_working
    self.experiences.previously_working.collect(&:company).join(",")
  end

  private

  def set_primary_email
    return if email.blank?
    Email.create(:address => email, :primary => true, :user_id => self.id, :confirmed => true)
  end

  def add_profile
    profile = self.build_profile
    profile.save(:validate => false)
  end

  def subscribe_me_for_news_letter
    if self.subscribe_to_newsletter == "1"
      Mailchimp.subscribe!(self.email, Mailchimp::LIST_NAME)
    end
  end

end

class Notification < ActionMailer::Base

  add_template_helper(ApplicationHelper)

  default from: Devise.mailer_sender

  def email_confirmation(email, ip)
    @content_type = "text/html"
    @email = email
    @user  = email.user
    @ip    = ip
    mail(:to => email.address, :subject => "Confirm your email")
  end

  def password_update(user, ip)
    @user  = user
    @ip    = ip
    mail(:to => user.email, :subject => "Password update")
  end

  def send_password_to_recruiter(recruiter, password)
    @user  = recruiter
    @password    = password
    mail(:to => @user.email, :subject => "Welcome to Postbord")
  end

  def password_instructions(user)
    @user  = user
    mail(:to => user.email, :subject => "Set up password")
  end

end

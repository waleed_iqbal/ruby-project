class CertificatesController < ApplicationController

  layout "blank"

  before_filter :get_user

  def index
    @certificates = @user.certificates

    respond_to do |format|
      format.html
      format.json { render json: @certificates }
      format.js
    end
  end

  def show
    @certificate = Certificate.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @certificate }
    end
  end

  def new
    @certificate = Certificate.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @certificate }
    end
  end

  def edit
    @certificate = Certificate.find(params[:id])
  end

  def create
    @certificate = Certificate.new(params[:certificate])
    @certificate.user_id = params.delete(:profile_id)
    respond_to do |format|
      if @certificate.save
        format.html
        format.json { render json: @certificate, status: :created, location: @certificate }
      else
        format.html { render action: "new" }
        format.json { render json: @certificate.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @certificate = Certificate.find(params[:id])
    @certificate.user_id = params.delete(:profile_id)
    respond_to do |format|
      if @certificate.update_attributes(params[:certificate])
        format.html
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @certificate.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @certificate = Certificate.find(params[:id])
    @certificate.destroy

    respond_to do |format|
      format.html
      format.json { head :no_content }
    end
  end

  def remove
    @certificate = Certificate.find(params[:id])
  end

  def remove_attachment
    @certificate = Certificate.find(params[:id])
    @certificate.attachment.clear
    @certificate.save
  end

  def preview
    @certificate = Certificate.find(params[:id])
  end

  private

  def get_user
    @user = User.find params[:profile_id]
    return redirect_to root_path, :alert => "Unauthorized access" unless @user == current_user
  end

end

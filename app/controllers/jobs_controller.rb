class JobsController < ApplicationController

  before_filter :authenticate_user!
  before_filter :ensure_user_is_recuruiter, :except => [:index,:show]
  before_filter :ensure_job_belongs_to_current_user, :only => [:edit, :udpate]

  def index
    @jobs = Job.all
    @jobs = @jobs.where(:user_id => current_user.id) if recruiter?
    @jobs = @jobs.where(:status => "active", :status => "inactive") unless recruiter?
    respond_to do |format|
      format.html
      format.json { render json: @jobs }
    end
  end

  def show
    @job = Job.find(params[:id])

    respond_to do |format|
      format.html
      format.json { render json: @job }
    end
  end

  def new
    @job = Job.new

    setup_nested_records

    respond_to do |format|
      format.html
      format.json { render json: @job }
    end
  end

  def edit
  end

  def create
    @job = Job.new(params[:job])

    respond_to do |format|
      extract_dates
      return job_preview if params[:preview]
      return save_draft if params[:save_as_draft]
      @job.user = current_user
      set_job_status
      if @job.save
        @job.post_to_social(:url => job_url(@job)) if current_user.is_premium?
        log_job
        format.html { redirect_to jobs_url, notice: 'Job was successfully created.' }
        format.json { render json: @job, status: :created, location: @job }
      else
        format.html { render action: "new" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @job = Job.find(params[:id])

    @job.attributes = params[:job]
    @job.user = current_user

    respond_to do |format|
      extract_dates
      return job_preview if params[:preview]
      return save_draft if params[:save_as_draft]
      should_post = current_user.is_premium? && @job.has_changed_draft_status?
      set_job_status
      if @job.save
        @job.post_to_social(:url => job_url(@job)) if should_post
        log_job
        format.html { redirect_to jobs_url, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @job = Job.find(params[:id])
    @job.destroy

    respond_to do |format|
      format.html { redirect_to jobs_url }
      format.json { head :no_content }
    end
  end

  private

  def ensure_user_is_recuruiter
    return redirect_to profile_path(current_user) unless current_user.recruiter?
  end

  def ensure_job_belongs_to_current_user
    @job = Job.find(params[:id])
    return redirect_to profile_path(current_user), :alert => "You are not authorized to perform this action" unless current_user == @job.user
  end

  def setup_nested_records
    @job.job_industries.build(:industry_id => current_user.try(:industry_id))
    @job.job_roles.build
  end

  def save_draft
    set_draft_status
    redirect_to edit_job_path(@job), notice: 'Job was successfully saved as draft.'
  end

  def job_preview
    set_draft_status
    render :template => "jobs/preview"
  end

  def extract_dates
    @job.start_date = extract_date_from(params[:job].delete(:start_date))
    @job.end_date = extract_date_from(params[:job].delete(:end_date))
  end

  def set_draft_status
    @job.status = "draft"
    @job.user = current_user
    log_job
    true if @job.save
  end

  def log_job
    log_job_details(
      :job_id => @job.id,
      :job_title => @job.title,
      :job_status => @job.status,
      :job_description => @job.description,
      :amount => @job.package,
      :application_url => @job.application_url,
      :skills => @job.desired_skills,
      :job_created => @job.created_at,
      :start_date => @job.start_date,
      :end_date => @job.end_date,
      :activity_klass => "JobActivity",
      :experience_level => @job.experience_level.title,
      :recruiter_name => @job.user.name,
      :recruiter_email => @job.user.email,
      :job_function => @job.job_functions.first.title,
      :industry => (@job.industries.all? &:blank?) == false ? @job.industries.first.title : "",
      :country => @job.country.present? ? @job.country.name : "",
      :company_name => @job.company.present? ? @job.company.name : "",
      :employment_type => @job.employment_type.present? ? @job.employment_type.title : "")
  end

  def today_date
    Time.now.strftime("%d/%m/%Y")
  end

  def set_job_status
    if @job.start_date.present? && @job.end_date.present?
      if @job.start_date <= today_date && @job.end_date >= today_date
        @job.status = "active"
      else
        @job.status = "inactive"
      end
    else
      @job.status = "inactive"
    end
  end

end

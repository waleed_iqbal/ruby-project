class SuggestionsController < ApplicationController

  skip_before_filter :authenticate_user!

  def index
    respond_to do |format|
      format.html do
        regex = Regexp.new(".*" + params[:queryString].strip + ".*", true)
        @jobs = Job.active.suggestions(regex).page(1).per(Job::SUGGESTION_LIMIT)
        @raw_results = @jobs.collect do |job|
          [:title, :desired_skills, :description].collect do |attr|
            data = job.send(attr)
            if attr.to_s == "title"
              data if data.match(regex)
            else
              data.split(" ").collect{|s| s if s.match(regex)}.compact.uniq if data.match(regex)
            end
          end
        end.flatten.compact
        @suggestions = @raw_results.collect{|r| r.downcase.strip.gsub(/[\<p\>\,]/,"")}.uniq
        render :layout => false
      end
      format.json do
        if params[:from].present? && params[:for].present?
          klass = params[:from].constantize
          regex = Regexp.new(".*" + params[:term] + ".*", true)
          results = klass.any_of({ params[:for].to_sym => regex }).uniq_by{|r| r.send(params[:for])}
          json = results.collect do |r|
            name = r.send(params[:for])
            {:id => r.id, :label => name, :value => name}
          end
          render :text => json.to_json
        end
      end
    end
  end

end

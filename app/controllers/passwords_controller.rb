class PasswordsController < Devise::PasswordsController

  after_filter :log_activity, :only => [:update]
  skip_before_filter :require_no_authentication

  def breach
    @user = User.where(:update_token => params[:token]).first
    log_user_activity(:activity_type => 'breached account', :activity_mode => 'password update')
    @user.update_reset_token!
    redirect_to edit_user_password_url(@user, :reset_password_token => @user.reset_password_token)
  end

  def after_sending_reset_password_instructions_path_for(resource_name)
    flash.delete(:notice)
    page_path('password_confirmation')
  end

  def log_activity
    log_user_activity(:activity_type => 'reset password')
  end

end

class ProfilesController < ApplicationController

  before_filter :get_user, :except => [:extra]

  def show
    return render :template => "profiles/display" unless @user == current_user
  end

  def edit_image
    render :layout => "blank"
  end

  def update
    respond_to do |format|
      @user.skip_reconfirmation!
      update_authentication_info if cookies[:social_network].present? && params[:social_connect].is?("completed")
      if @user.update_attributes(params[:user])
        cookies[:social_connect] = params[:social_connect] if params[:social_connect].present?
        @user.send_password_instructions
        log_user_activity(:activity_mode => params[:social_connect], :activity_type => "social welcome")
        @valid = true
        format.js
      else
        @valid = false
        format.js
        format.html{redirect_to profile_path(@user)}
      end
    end
  end

  def update_image
    if params[:image_id].blank?
      @user.avatar = params[:file]
    else
      @image = Image.find params[:image_id]
      @user.avatar = @image.photo
    end
    return redirect_to edit_image_profile_path(@user) unless @user.valid?
    @user.process!(params)
    @user.save
  end

  def remove_avatar
    @user.avatar.clear
    @user.save
  end

  def extra
    @user = User.find params[:profile_id]
    method   = params[:id]
    render :template => method
  end

  private

  def get_user
    @user = User.find params[:id]
    @profile = @user.profile
  end

  def update_authentication_info
    email = Email.where(:address => params[:user][:email]).first
    authentication = @user.authentications.where(:provider => cookies[:social_network]).first
    if authentication.present? && email.present?
      authentication.user = email.user
      authentication.save
      sign_out @user
      @user = authentication.user
      sign_in @user
    end
  end

end

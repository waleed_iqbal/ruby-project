class MainController < ApplicationController

  skip_before_filter :authenticate_user!, :except => [:index]

  def login
    render :template => "shared/login_popup", :layout => "blank"
  end

  def signup
    render :template => "shared/signup_popup", :layout => "blank"
  end

  def forgot_password
  end

  def confirmation_instructions
  end

  def email_validation
    respond_to do |format|
      format.json do
        return render json: {validated: false, :message => 'Please enter the required field'} if params[:email].blank?
        @users = User.where(:email => params[:email])
        user_present = !@users.count.zero?
        if user_present
          render json: {validated: false, :message => 'Email is already registered!'}
        else
          render json: {validated: true}
        end
      end
    end
  end

  def set_cookie
    cookies[:social_connect] = params[:value]
    log_user_activity(:activity_mode => params[:value], :activity_type => "social welcome")
    return redirect_to(profile_path(current_user))
  end

  def set_reminder_cookies
    cookies[:incomplete_profile] = params[:value]
    log_user_activity(:activity_mode => params[:value], :activity_type => "profile reminder")
    return redirect_to(profile_path(current_user))
  end

  def activities
    @activities = Activity.all.group_by(&:user_id)
    respond_to do |format|
      format.json do
        json = @activities.collect do |user_id, activities|
          {
            :user_id => user_id.to_s,
            :activities => activities
          }
        end
        render :text => JSON.pretty_generate({:user_activities => json })
      end
    end
  end

  def extra
    controller.send params[:action]
  end

  def show
    begin
      render :template => "main/#{params[:page]}"
    rescue Exception => e
      render_404
    end
  end

  def iframe_login
    @location = stored_location_for(params[:resource]) || profile_path(params[:resource]) || root_path
    render :layout => false
  end

  def subscribe_to_newsletter
    @subscription = Mailchimp.subscribe!(params[:email], Mailchimp::LIST_NAME)
    respond_to do |format|
      format.js
    end
  end

  private

  def render_404
    respond_to do |format|
      format.html { render :file => "#{Rails.root}/public/404", :layout => false, :status => :not_found }
      format.xml  { head :not_found }
      format.any  { head :not_found }
    end
  end

end

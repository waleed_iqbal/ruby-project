class SocialController < ApplicationController

  skip_before_filter :authenticate_user!

  def integration
    send(params[:action])
  end

  def linkedin
    @client = LinkedIn::Client.new(APP_CONFIG[:linkedin][:consumer_key], APP_CONFIG[:linkedin][:consumer_secret])
    request_token = @client.request_token(:oauth_callback => LINKEDIN_CALLBACK)
    session[:linkedin_client] = @client
    session[:rtoken] = request_token.token
    session[:rsecret] = request_token.secret

    redirect_to @client.request_token.authorize_url
  end

  def linkedin_callback
    @client = session.delete(:linkedin_client)
    if params[:oauth_verifier].present?
      @auth_token, @auth_secret = @client.authorize_from_request(session[:rtoken], session[:rsecret], params[:oauth_verifier])
      @profile = @client.profile(:fields => ['email-address','first-name','last-name'])
      data = {:auth_token => @auth_token, :auth_secret => @auth_secret, :user => @profile.to_hash.symbolize_keys }
      update_authentication_information('linkedin', data)
    else
      return redirect_to new_user_registration_path, :alert => params[:oauth_problem].try(:titleize) || "User cancelled authentication"
    end
  end

  def facebook
    redirect_to FACEBOOK_AUTH.url_for_oauth_code(:permissions => 'email')
  end

  def facebook_callback
    if params[:error]
      return redirect_to new_user_registration_path, :alert => params[:error_reason].titleize
    else
      access_token = FACEBOOK_AUTH.get_access_token(params[:code])
      @graph = Koala::Facebook::API.new(access_token)

      profile = @graph.get_object('me')
      data = Hashie::Mash.new profile
      data.access_token = access_token
      update_authentication_information('facebook',data)
    end
  end

  def twitter
      @request_token = TWITTER_CLIENT.request_token(:oauth_callback => TWITTER_CONFIRM_URl)
      session[:request_token] = @request_token
      redirect_to @request_token.authorize_url
  end

  def twitter_callback
    if params[:denied]
      return redirect_to new_user_registration_path, :alert => 'User denied access'
    else
      @request_token = session[:request_token]
      @access_token = TWITTER_CLIENT.authorize(
        @request_token.token,
        @request_token.secret,
        :oauth_verifier => params[:oauth_verifier]
      )
      session.delete(:request_token)
      data = Hashie::Mash.new @access_token.params
      update_authentication_information('twitter', data)
    end
  end

  def google
    plus = GOOGLE_CLIENT.discovered_api('plus')

    redirect_uri = GOOGLE_CLIENT.authorization.authorization_uri.to_s
    redirect_to redirect_uri
  end

  def google_callback
    if params[:error]
      return redirect_to new_user_registration_path, :alert => params[:error].titleize
    else
      GOOGLE_CLIENT.authorization.code = params[:code]
      token = GOOGLE_CLIENT.authorization.fetch_access_token!

      update_google_user_info(token.symbolize_keys)
    end
  end

  protected

  def update_authentication_information(provider, data)
    data[:existing_user] = current_user if user_signed_in?
    @user, activity_type = Authentication.from_provider(provider, data)
    set_social_cookies(provider)
    log_user_activity(:activity_mode => provider, :activity_type => activity_type, :user => @user)
    if user_signed_in?
      return redirect_to accounts_path
    else
      sign_in @user
      redirect_to profile_path(@user)
    end
  end

  def update_google_user_info(token)
     response = HTTParty.get("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=#{token[:access_token]}")
     data = JSON.parse(response.body)
     update_authentication_information('google', data.symbolize_keys_recursive)
  end

  def set_social_cookies(provider)
    cookies.delete(:social_connect) if cookies[:social_connect] == "skipped"
    cookies[:social_network] = provider
  end

end

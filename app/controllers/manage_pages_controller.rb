class ManagePagesController < ApplicationController

  before_filter :authenticate_user!
  before_filter :check_authentication_session, :except => [:require_authentication, :authenticate]

  def require_authentication
  end

  def authenticate
    if params[:username] == MANAGE_PAGES_CONFIG[:username] && params[:password] == MANAGE_PAGES_CONFIG[:password]
      session[:page_manager] = true
      redirect_to manage_pages_path
    else
      redirect_to :back, :alert => "Please enter valid information"
    end
  end

  def index
  end

  def facebook
    @callback_url = "http://#{APP_CONFIG[:domain]}/manage_pages/redirect_from_facebook"
    oauth = session[:oauth] = Koala::Facebook::OAuth.new(APP_CONFIG[:facebook][:api_key], APP_CONFIG[:facebook][:app_secret], @callback_url)
    url = oauth.url_for_oauth_code(:permissions => "manage_pages,publish_stream")
    redirect_to url
  end

  def redirect_from_facebook
    @access_token_info = session[:oauth].get_access_token_info(params[:code])
    Setting.create(:key => "access_token",:value => @access_token_info['access_token'])
    redirect_to manage_pages_path
  end

  def twitter
    @request_token = TWITTER_CLIENT.request_token(:oauth_callback => TWITTER_PAGE_CONFIRM_URL)
    session[:request_token] = @request_token
    redirect_to @request_token.authorize_url
  end

  def twitter_callback
    if params[:denied]
      return redirect_to new_user_registration_path, :alert => 'User denied access'
    else
      @request_token = session[:request_token]
      @access_token = TWITTER_CLIENT.authorize(
        @request_token.token,
        @request_token.secret,
        :oauth_verifier => params[:oauth_verifier]
      )
      session.delete(:request_token)
      Setting.create(:key => "oauth_token",:value => @access_token.params[:oauth_token])
      Setting.create(:key => "oauth_token_secret",:value => @access_token.params[:oauth_token_secret])
      redirect_to manage_pages_path
    end
  end

  private

  def check_authentication_session
    return redirect_to require_authentication_manage_pages_path unless session[:page_manager]
  end

end
class AccountsController < ApplicationController

  before_filter :get_user

  def index
  end

  def edit
    @profile = @user.profile
  end

  def update
    respond_to do |format|
      @profile.attributes = params[:profile]
      @profile.phone.gsub!(/\A0+/,"")
      @user.attributes    = params[:user]
      if @user.save && @profile.save
        log_user_activity(:activity_type => 'settings', :activity_mode => 'profile')
        format.html { return redirect_to profile_path(@user), :notice => "Updated Successfully."}
        format.json {render :json => {:success => 1}}
      else
        format.html{render :edit}
        format.json do
          errors = @profile.errors.collect do |attribute, message|
            {:attribute => ".#{attribute}", :message => message}
          end
          render :json => {:success => 0, :errors => errors}
        end
      end
    end
  end

  private

  def get_user
    @user = current_user
    @profile = @user.profile
  end

end

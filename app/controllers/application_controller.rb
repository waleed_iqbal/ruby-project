class ApplicationController < ActionController::Base

  include Rack::Recaptcha::Helpers

  protect_from_forgery

  before_filter :log_user_back_in, :except => [:update]
  before_filter :authenticate_user!
  before_filter :get_browser_details
  before_filter :set_new_password

  after_filter :set_password_updated, if: :devise_controller?

  helper_method :development?, :recruiter?

  def log_user_activity(options = {})
    @user_agent = UserAgent.parse(request.user_agent)
    activity = Activity.new options
    current_user ||= options.delete(:user)
    attributes = {:user_id => current_user.try(:id), :os => @user_agent.os  ,:browser => @user_agent.browser, :ip => request.ip, :country => country_name_from_ip, :city => city_name_from_ip, :area => area_name_from_ip }.merge(options)
    activity.attributes = attributes
    activity.save
  end

  def log_classified_activitiy(options = {})
    search = options.delete(:activity_klass)
    activity = search.constantize.new options.clean
    activity.save
  end

  def log_search_activity(options = {})
    log_classified_activitiy(options)
  end

  def log_job_details(options = {})
    log_classified_activitiy(options)
  end

  def after_sign_in_path_for(resource)
    extras_path(:iframe_login, :resource => resource)
  end

  def get_browser_details
    @user_agent = UserAgent.parse(request.user_agent)
  end

  def development?
    Rails.env.development?
  end

  def country_name_from_ip(ip = nil)
    country = GEO_IP.country(ip || request.ip)
    country.try :country_name
  end

  def city_name_from_ip(ip = nil)
    city = GEO_IP.city(ip || request.ip)
    city.try :city_name
  end

  def area_name_from_ip(ip = nil)
    city = GEO_IP.city(ip || request.ip)
    city.try :real_region_name
  end

  def log_user_back_in
    return if session[:needs_to_login].blank? || devise_controller?
    @user = User.find session[:needs_to_login]
    sign_in(@user)
    session.delete(:needs_to_login)
  end

  def extract_date_from(date_string)
    return nil if date_string.blank?
    if date_string.split("/").length < 3
      Date.parse(date_string.gsub("/","-"))
    else
      Date.strptime(date_string, "%d/%m/%Y")
    end
  end

  def recruiter?
    current_user.try(:recruiter?)
  end

  private

  def set_password_updated
    if controller_name == "passwords" && action_name == "update"
      return if session[:id_for_password_update].blank?
      @user = User.find session.delete(:id_for_password_update)
      sign_in(@user)
      log_user_activity(:activity_type => 'recruiter account', :activity_mode => 'password update')
    end
  end

  def set_new_password
    return unless current_user.try(:recruiter?)
    unless current_user.password_updated
      current_user.update_reset_token!
      session[:needs_to_login] = session[:id_for_password_update] = current_user.id
      @user = current_user
      sign_out(current_user)
      redirect_to edit_user_password_url(@user, :reset_password_token => @user.reset_password_token)
    end
  end

end

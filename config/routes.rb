PocApp::Application.routes.draw do

  devise_for :users, :controllers => { :registrations => :registrations, :passwords => :passwords, :sessions => :sessions, :confirmations => :confirmations }

  devise_scope :user do
    put "password/:scope", :to => "devise/passwords#update", :as => :update_password
    get '/password/breach/:token' => 'passwords#breach', :as => :password_breach
  end

  resources :profiles, :only => [:edit, :update, :show] do
    member do
      get :edit_image
      post :update_image
      delete :remove_avatar
    end
    resources :experiences do
      member do
        get :remove
      end
    end
    resources :certificates do
      member do
        get :remove
        delete :remove_attachment
        get :preview
      end
    end
    resources :educations do
      member do
        get :remove
      end
    end
    resources :skills, :only => [:index] do
      collection do
        get :edit
        post :update
      end
    end
    resources :emails do
      member do
        post :make_primary
      end
      collection do
        get  :confirmation
        get  :breach
      end
    end
  end

  resources :suggestions, :only => [:index]
  resources :recruiters do
    collection do
      get :confirmation
    end
  end

  resources :recruiter_sessions, :only => [:new, :create]
  resources :accounts

  resources :searches
  resources :jobs
  resources :manage_pages do
    collection do
      get :require_authentication
      get :authenticate
      get :facebook
      get :redirect_from_facebook
      get :twitter
      get :twitter_callback
    end
  end

  match 'email_validation' => 'main#email_validation', :via => :get, :as => "email_validation"
  match 'social/:action', :via => :get, :to => "social#integration", :as => :social
  match 'profiles/:profile_id/:id', :via => :get, :to => "profiles#extra", :as => :profile_extra
  match 'main/:action', :via => [:get, :post], :to => "main#extra", :as => :extras
  match 'images/upload_image', :via => [:post], :to => "images#upload_image", :as => :upload_image
  match 'login', :via => [:get], :to => "main#login", :as => :login
  match 'signup', :via => [:get], :to => "main#signup", :as => :signup
  match 'forgot_password', :via => [:get], :to => "main#forgot_password", :as => :forgot_password
  match 'confirmation_instructions', :via => [:get], :to => "main#confirmation_instructions", :as => :confirmation_instructions

  get '/activities(.:format)' => 'main#activities', :as => :activities
  get '/:page' => 'main#show', :as => :page

  root :to => 'accounts#index'

end
